# Install to /usr unless otherwise specified, such as `make PREFIX=/app`
PREFIX=/usr

# variables
URI_PREFIX=org.loers
PKG_NAME=Remode
BIN_NAME=remode

# What to run to install various files
INSTALL=install
# Run to install the actual binary
INSTALL_PROGRAM=$(INSTALL)
# Run to install application data, with differing permissions
INSTALL_DATA=$(INSTALL) -m 644

# Directories into which to install the various files
bindir=$(DESTDIR)$(PREFIX)/bin
sharedir=$(DESTDIR)$(PREFIX)/share

base_dir=$(realpath .)
flatpak_build_dir=$(base_dir)/.flatpak


.PHONY: clean prepare install debug build_aarch64
	
# Install in a flatpak system
install:
	ls
	# @cargo build --release
	# @echo "Create bin dir"
	# @mkdir -p $(bindir)
	
	# @echo "Install binary"
	# @$(INSTALL_PROGRAM) target/release/$(BIN_NAME) $(bindir)/$(URI_PREFIX).$(PKG_NAME)
	
	# @echo "Install icons"
	# @mkdir -p $(sharedir)/icons/hicolor/scalable/apps/
	# @mkdir -p $(sharedir)/icons/hicolor/64x64/apps/
	# @mkdir -p $(sharedir)/icons/hicolor/128x128/apps/
	# @mkdir -p $(sharedir)/applications/
	# @mkdir -p $(sharedir)/metainfo/
	# @$(INSTALL_DATA) data/$(URI_PREFIX).$(PKG_NAME).svg $(sharedir)/icons/hicolor/scalable/apps/$(URI_PREFIX).$(PKG_NAME).svg
	# @$(INSTALL_DATA) data/$(URI_PREFIX).$(PKG_NAME).64.png $(sharedir)/icons/hicolor/64x64/apps/$(URI_PREFIX).$(PKG_NAME).png
	# @$(INSTALL_DATA) data/$(URI_PREFIX).$(PKG_NAME).128.png $(sharedir)/icons/hicolor/128x128/apps/$(URI_PREFIX).$(PKG_NAME).png
	# @echo "Force icon cache refresh"
	# @touch $(sharedir)/icons/hicolor

	# @echo "Install application meta-data"
	# @mkdir -p $(sharedir)/metainfo
	# @$(INSTALL_DATA) data/$(URI_PREFIX).$(PKG_NAME).appdata.xml $(sharedir)/metainfo/$(URI_PREFIX).$(PKG_NAME).appdata.xml

	# @echo "Install desktop file"
	# @$(INSTALL_DATA) data/$(URI_PREFIX).$(PKG_NAME).desktop $(sharedir)/applications/$(URI_PREFIX).$(PKG_NAME).desktop

	# @echo "SUCCESS"

build:
	@echo Building in $(flatpak_build_dir)/host
	@flatpak build \
		--share=network \
		--filesystem=$(flatpak_build_dir)/host \
		--env=PATH=$$PATH:/usr/lib/sdk/rust-stable/bin \
		--env=CARGO_HOME=/run/build/Remode/cargo \
        --env=RUST_BACKTRACE=1 \
		$(flatpak_build_dir)/host \
		cargo build

debug:
	@flatpak build \
		--with-appdir \
		--allow=devel \
		--socket=fallback-x11 \
		--socket=wayland \
		--device=dri \
		--talk-name=org.a11y.Bus \
		--env=RUST_LOG=remode=debug \
		--env=G_MESSAGES_DEBUG=none \
		--env=RUST_BACKTRACE=1 \
		--talk-name='org.freedesktop.portal.*' \
		--talk-name=org.a11y.Bus \
		--env=XDG_SESSION_TYPE=x11 \
		--env=LANG=en_US.UTF-8 \
		--env=COLORTERM=truecolor \
		$(flatpak_build_dir)/host \
		target/debug/remode

# flatpak-builder --arch=aarch64 --state-dir=target/.flatpak-builder --force-clean $(flatpak_build_dir)/aarch64 data/$(URI_PREFIX).$(PKG_NAME).yml	
build_aarch64:
	@echo Building in $(flatpak_build_dir)/aarch64
	@flatpak build \
		--share=network \
		--filesystem=$(flatpak_build_dir)/aarch64 \
		--env=PATH=$$PATH:/usr/lib/sdk/rust-stable/bin \
		--env=CARGO_HOME=/run/build/Remode/cargo \
        --env=RUST_BACKTRACE=1 \
		--env=CARGO_TARGET_DIR=target/aarch64 \
		$(flatpak_build_dir)/aarch64 \
		ls
		# cargo build --release

clean:
	@cargo clean
	@rm -rf .flatpak
	@flatpak build-init \
		--sdk-extension=org.freedesktop.Sdk.Extension.rust-stable \
		$(flatpak_build_dir)/host \
		org.loers.Remode \
		org.gnome.Sdk \
		org.gnome.Platform \
		40
	@flatpak build-init \
		--sdk-extension=org.freedesktop.Sdk.Extension.rust-stable \
		$(flatpak_build_dir)/aarch64 \
		org.loers.Remode \
		org.gnome.Sdk/aarch64 \
		org.gnome.Platform/aarch64 \
		40

build_deps:
	flatpak-builder \
		--ccache \
		--force-clean \
		--disable-updates \
		--disable-download \
		--build-only \
		--keep-build-dirs \
		--state-dir=$(base_dir)/target/.flatpak-builder \
		--stop-at=$(BIN_NAME) \
		$(flatpak_build_dir)/.flatpak/host \
		$(base_dir)/data/$(URI_PREFIX).$(PKG_NAME).yml

prepare:
	# apt -y install qemu-user-static
	flatpak install flathub org.gnome.Sdk//40
	flatpak install flathub org.gnome.Platform//40
	flatpak install flathub org.gnome.Sdk/aarch64/40
	flatpak install flathub org.gnome.Platform/aarch64/40
	flatpak install flathub org.freedesktop.Sdk.Extension.rust-stable//20.08

	https://mirror.autisten.club/arch4edu/$arch