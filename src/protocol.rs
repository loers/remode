use bitflags::bitflags;
use byteorder::ByteOrder;

#[derive(Debug)]
pub struct Packet {
    mouse_x: i16,
    mouse_y: i16,
    flags: Flags,
    key: u16,
    scroll_y: i16,
}

bitflags! {
    pub struct Flags: u16 {
        const MOUSE_LEFT_DOWN = /* */ 0b00000001;
        const MOUSE_LEFT_UP = /*   */ 0b00000010;
    }
}

impl Packet {
    pub fn parse(data: &[u8; 16]) -> Self {
        let mouse_x = byteorder::LittleEndian::read_i16(&data[0..2]);
        let mouse_y = byteorder::LittleEndian::read_i16(&data[2..4]);
        let flags = byteorder::LittleEndian::read_u16(&data[4..6]);
        let key = byteorder::LittleEndian::read_u16(&data[6..8]);
        let scroll_y = byteorder::LittleEndian::read_i16(&data[8..10]);

        Packet {
            mouse_x,
            mouse_y,
            flags: Flags::from_bits(flags).unwrap_or(Flags::empty()),
            key,
            scroll_y,
        }
    }

    pub fn has_flag(&self, flag: Flags) -> bool {
        self.flags.contains(flag)
    }

    pub fn set_mouse(&mut self, x: i16, y: i16) {
        self.mouse_x = x;
        self.mouse_y = y;
    }

    pub fn mouse(&self) -> (i16, i16) {
        (self.mouse_x, self.mouse_y)
    }

    pub fn set_flags(&mut self, flags: Flags) {
        self.flags = flags;
    }

    pub fn flags(&self) -> &Flags {
        &self.flags
    }

    pub fn flags_mut(&mut self) -> &mut Flags {
        &mut self.flags
    }

    pub fn set_key(&mut self, value: u16) {
        self.key = value;
    }

    pub fn key(&self) -> Option<u16> {
        if self.key == 0 {
            None
        } else {
            Some(self.key)
        }
    }

    pub fn set_scroll_y(&mut self, value: i16) {
        self.scroll_y = value
    }

    pub fn scroll_y(&self) -> i16 {
        self.scroll_y
    }

    pub fn as_bytes(&self) -> [u8; 16] {
        let mut buf = [0u8; 16];
        byteorder::LittleEndian::write_i16(&mut buf[0..2], self.mouse_x);
        byteorder::LittleEndian::write_i16(&mut buf[2..4], self.mouse_y);
        byteorder::LittleEndian::write_u16(&mut buf[4..6], self.flags.bits());
        byteorder::LittleEndian::write_u16(&mut buf[6..8], self.key);
        byteorder::LittleEndian::write_i16(&mut buf[8..10], self.scroll_y);
        buf
    }
}

impl Default for Packet {
    fn default() -> Self {
        Packet {
            mouse_x: 0,
            mouse_y: 0,
            flags: Flags::empty(),
            key: 0,
            scroll_y: 0,
        }
    }
}
