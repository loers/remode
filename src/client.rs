use std::{net::UdpSocket, sync::{mpsc::{Sender, channel}}};
use pinesense_rs;
use crate::{protocol::{Packet, Flags}, settings::Settings};

pub enum Command {
    Update(Settings),
    Key(u16),
    MouseLeftDown,
    MouseLeftUp,
    Scroll(i16),
}

pub fn start(mut settings: Settings) -> Sender<Command> {
    let (sender, receiver) = channel();
    std::thread::spawn(move || {
        let mut imu = pinesense_rs::IMU::init();
        let socket = UdpSocket::bind("0.0.0.0:0").unwrap();
        socket.set_nonblocking(true).unwrap();
        
        loop {
            let mut packet = Packet::default();

            if let Ok(command) = receiver.try_recv() {
                match command {
                    Command::Update(s) => {
                        settings = s;
                    },
                    Command::MouseLeftDown => {
                        packet.flags_mut().toggle(Flags::MOUSE_LEFT_DOWN);
                    }
                    Command::MouseLeftUp => {
                        packet.flags_mut().toggle(Flags::MOUSE_LEFT_UP);
                    }
                    Command::Key(keycode) => {
                        packet.set_key(keycode);
                    }
                    Command::Scroll(scroll) => {
                        packet.set_scroll_y(scroll);
                    }
                }
            }
            
            let (x, y) = if imu.is_some() {
                let data = imu.as_mut().unwrap().read();
                (data[5] * -1, data[4])
            } else {
                (0, 0)
            };
            
            packet.set_mouse(x, y);
            
            if packet.flags().contains(Flags::MOUSE_LEFT_DOWN) {
                packet.set_mouse(0, 0)
            }

            socket.send_to(&packet.as_bytes(), settings.ip()).ok();
        }
    });
    return sender;
}