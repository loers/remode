#include "prepare.h"
#include <linux/uinput.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>

int prepare() {
   struct uinput_setup usetup;

   int fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK);

   /* enable mouse button left and relative events */
   ioctl(fd, UI_SET_EVBIT, EV_KEY);
   ioctl(fd, UI_SET_KEYBIT, BTN_LEFT);
   
   ioctl(fd, UI_SET_KEYBIT, KEY_ENTER);
   
   ioctl(fd, UI_SET_KEYBIT, KEY_MUTE);
   ioctl(fd, UI_SET_KEYBIT, KEY_VOLUMEDOWN);
   ioctl(fd, UI_SET_KEYBIT, KEY_VOLUMEUP);
   ioctl(fd, UI_SET_KEYBIT, KEY_PLAYPAUSE);
   ioctl(fd, UI_SET_KEYBIT, KEY_PREVIOUSSONG);
   ioctl(fd, UI_SET_KEYBIT, KEY_NEXTSONG);
   ioctl(fd, UI_SET_KEYBIT, KEY_HOMEPAGE);

   ioctl(fd, UI_SET_KEYBIT, KEY_UP);
   ioctl(fd, UI_SET_KEYBIT, KEY_DOWN);
   ioctl(fd, UI_SET_KEYBIT, KEY_LEFT);
   ioctl(fd, UI_SET_KEYBIT, KEY_RIGHT);
   
   ioctl(fd, UI_SET_KEYBIT, KEY_SCROLLUP);
   ioctl(fd, UI_SET_KEYBIT, KEY_SCROLLDOWN);

   ioctl(fd, UI_SET_EVBIT, EV_REL);
   ioctl(fd, UI_SET_RELBIT, REL_X);
   ioctl(fd, UI_SET_RELBIT, REL_Y);

   memset(&usetup, 0, sizeof(usetup));
   usetup.id.bustype = BUS_USB;
   usetup.id.vendor = 0x1234; /* sample vendor */
   usetup.id.product = 0x5678; /* sample product */
   strcpy(usetup.name, "Example device");

   ioctl(fd, UI_DEV_SETUP, &usetup);
   ioctl(fd, UI_DEV_CREATE);
   
   sleep(1);
   return fd;
}