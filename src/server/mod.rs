use input_linux_sys::*;
use libc::c_void;
use libc::input_event;
use libc::timeval;
use std::mem::size_of;
use std::net::UdpSocket;

use crate::protocol::{Flags, Packet};

extern "C" {
    fn prepare() -> i32;
}

pub fn start() -> std::io::Result<()> {
    let fd = unsafe { prepare() };

    let socket = UdpSocket::bind("0.0.0.0:4389")?;

    let mut key = None;
    let mut move_mouse = 0;
    loop {
        let mut buf = [0; 16];
        if let Err(e) = socket.recv_from(&mut buf) {
            println!("{:?}", e);
            break;
        }
        let packet = Packet::parse(&buf);

        // mouse clicking
        {
            if packet.flags().contains(Flags::MOUSE_LEFT_DOWN) {
                emit(fd, EV_KEY, BTN_LEFT as i32, 1);
                move_mouse = 10;
            }
        }

        // mouse moving
        if move_mouse == 0 {
            let (x, y) = packet.mouse();
            let a = (x as f64).tanh() * (x as f64).abs();
            let b = (y as f64).tanh() * (y as f64).abs();
            let x = a as i16;
            let y = b as i16;
            if x != 0 {
                emit(fd, EV_REL, REL_X, x as i32);
            }
            if y != 0 {
                emit(fd, EV_REL, REL_Y, y as i32);
            }
        }

        // mouse clicking
        {
            if move_mouse > 0 {
                move_mouse -= 1;
            } else {
                emit(fd, EV_KEY, BTN_LEFT as i32, 0);
            }
        }

        // scrolling
        {
            let y = packet.scroll_y();
            if y < 0 {
                emit(fd, EV_KEY, KEY_SCROLLDOWN, 1);
            } else if y > 0 {
                emit(fd, EV_KEY, KEY_SCROLLUP, 1);
            } else {
                emit(fd, EV_KEY, KEY_SCROLLUP, 0);
                emit(fd, EV_KEY, KEY_SCROLLDOWN, 0);
            }
        }

        // typing
        {
            if let Some(k) = packet.key() {
                println!("KEY {}", k);
                emit(fd, EV_KEY, k as i32, 1);
                key = Some(k);
            } else if let Some(k) = key {
                println!("UP  {}", k);
                emit(fd, EV_KEY, k as i32, 0);
                key = None;
            }
        }

        emit(fd, EV_SYN, SYN_REPORT, 0);
    }

    unsafe {
        libc::close(fd);
    }

    Ok(())
}

fn emit(fd: i32, type_: i32, code: i32, val: i32) {
    let ie = input_event {
        time: timeval {
            tv_sec: 0,
            tv_usec: 0,
        },
        type_: type_ as u16,
        code: code as u16,
        value: val,
    };
    unsafe {
        libc::write(
            fd,
            &ie as *const input_event as *const c_void,
            size_of::<input_event>(),
        );
    }
}
