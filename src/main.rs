#[macro_use]
extern crate clap;
extern crate serde;

use clap::{App, SubCommand};
use gtk::prelude::*;
use gtk::Application;
use crate::settings::Settings;

const APP_NAME: &str = "org.loers.Remode";

mod client;
mod ui {
    pub mod buttons;
    pub mod window;
}
mod server;
mod settings;
mod protocol;

fn main() {
    let matches = App::new("Remode")
        .version(crate_version!())
        .author(crate_authors!())
        .about(crate_description!())
        .subcommand(SubCommand::with_name("server").about("Start the server"))
        .subcommand(SubCommand::with_name("test").about("testing stuff"))
        .get_matches();
        
    if let Some(_m) = matches.subcommand_matches("server") {
        println!("Starting server");
        server::start().unwrap();
    } else {
        let settings = Settings::init();
        let send_command = client::start(settings.clone());
        let app = Application::builder().application_id(APP_NAME).build();
        app.connect_activate(move |app| {
            std::thread::sleep(std::time::Duration::from_millis(100));
            let window = ui::window::window(settings.clone(), app, send_command.clone());
            window.show();
        });
        app.connect_startup(|_app| {});
        app.run();
    }
}
