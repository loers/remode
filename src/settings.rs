use glib::home_dir;
use serde::{Deserialize, Serialize};
use std::cell::RefCell;
use std::fs::create_dir_all;
use std::fs::read_to_string;
use std::io::Write;

#[derive(Debug, Clone, Default, Deserialize, Serialize)]
pub struct Settings {
    ip: RefCell<String>,
}

impl Settings {
    pub fn init() -> Self {
        let mut settings_file = home_dir();
        settings_file.push(".remode");
        create_dir_all(&settings_file).unwrap();
        settings_file.push("settings.toml");

        if settings_file.exists() {
            let s = read_to_string(settings_file).unwrap();
            toml::de::from_str(&s).unwrap()
        } else {
            let settings = Settings::default();
            let mut f = std::fs::File::create(settings_file).unwrap();
            let default_settings = toml::ser::to_string(&settings).unwrap();
            f.write_all(default_settings.as_bytes()).unwrap();
            settings
        }
    }

    pub fn update(&self) {
        let mut settings_file = home_dir();
        settings_file.push(".remode");
        settings_file.push("settings.toml");

        let mut f = std::fs::File::create(&settings_file).unwrap();
        let s = toml::ser::to_string(self).unwrap();
        f.write_all(s.as_bytes()).unwrap();
    }

    pub fn set_ip(&self, ip: &str) {
        self.ip.replace(ip.into());
        println!("{:?} {:?}", self, ip);
        self.update()
    }

    pub fn ip(&self) -> String {
        self.ip.borrow().clone()
    }
}
