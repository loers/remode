use crate::client::Command;
use crate::settings::Settings;
use crate::ui::buttons::buttons;
use gdk4::BUTTON_PRIMARY;
use gio::SimpleAction;
use gtk::{BoxBuilder, ButtonBuilder, EntryBuilder, GestureClickBuilder, GesturePanBuilder, HeaderBarBuilder, Orientation, PropagationPhase, WindowHandleBuilder, prelude::*};
use gtk::{Application, ApplicationWindow};
use std::sync::{mpsc::Sender};

const TITLE: &str = "Remode";

const DEFAULT_WIDTH: i32 = 400;
const DEFAULT_HEIGHT: i32 = 600;

pub fn window(settings: Settings, application: &Application, sender: Sender<crate::client::Command>) -> ApplicationWindow {
    let window = ApplicationWindow::builder()
        .application(application)
        .default_width(DEFAULT_WIDTH)
        .default_height(DEFAULT_HEIGHT)
        .title(TITLE)
        .build();

    let action_toggle_entry = SimpleAction::new_stateful("toggle.input", None, &false.to_variant());
    window.add_action(&action_toggle_entry);

    let header = HeaderBarBuilder::new().visible(true).build();
    window.set_titlebar(Some(&header));
    let header_box = BoxBuilder::new().spacing(4).build();
    let button = ButtonBuilder::new().icon_name("application-menu").build();

    let entry = EntryBuilder::new()
        .hexpand(true)
        .visible(false)
        .text(&settings.ip())
        .build();
    header_box.append(&button);
    header_box.append(&entry);
    let header_bar = WindowHandleBuilder::new().child(&header_box).build();
    header_bar.set_parent(&header);

    {
        let sender = sender.clone();
        let entry = entry.clone();
        let action_toggle_entry = action_toggle_entry.clone();
        button.connect_clicked(move |b| {
            let state: bool = action_toggle_entry.state().unwrap().get().unwrap();
            if state {
                settings.set_ip(entry.text().as_str());
                sender.send(Command::Update(settings.clone())).ok();
            }
            action_toggle_entry.activate(None);
        });
    }

    {
        let entry = entry.clone();
        action_toggle_entry.connect_activate(move |a, _| {
            let state: bool = a.state().unwrap().get().unwrap();
            let new_state = !state;
            a.change_state(&new_state.to_variant());
            entry.set_visible(new_state);
        });
    }

    let root_box = BoxBuilder::new()
        .hexpand(true)
        .vexpand(true)
        .visible(true)
        .orientation(Orientation::Vertical)
        .build();

    let mouse_area = BoxBuilder::new()
        .visible(true)
        .hexpand(true)
        .vexpand(true)
        .build();
    root_box.append(&mouse_area);

    let buttons = buttons(sender.clone());
    root_box.append(&buttons);

    let pan_gesture_controller = GesturePanBuilder::new()
        .propagation_phase(PropagationPhase::Bubble)
        .touch_only(true)
        .build();

    let click_gesture_controller = GestureClickBuilder::new()
        .button(BUTTON_PRIMARY)
        .propagation_phase(PropagationPhase::Bubble)
        .touch_only(true)
        .build();
        
    mouse_area.add_controller(&pan_gesture_controller);
    mouse_area.add_controller(&click_gesture_controller);

    window.set_child(Some(&root_box));

    // handlers
    {
        let sender = sender.clone();
        pan_gesture_controller.connect_drag_update(move |_gp, x, y| {
            sender.send(Command::Scroll(x as i16)).ok();
        });
    }
    {
        let sender = sender.clone();
        click_gesture_controller.connect_end(move |_click, _b| {
            sender.send(Command::MouseLeftUp).ok();        
        });
    }
    {
        let sender = sender.clone();
        click_gesture_controller.connect_begin(move |_click, _b| {
            sender.send(Command::MouseLeftDown).ok();        
        });
    }
    {
        // let sender = sender.clone();
        click_gesture_controller.connect_pressed(move |_click, _b, x, y| {
            // println!("pressed {} {}", x, y);
        });
    }

    window
}
