use std::sync::mpsc::Sender;

use gtk::prelude::{BoxExt, ButtonExt, GridExt};
use input_linux_sys::{KEY_DOWN, KEY_HOMEPAGE, KEY_LEFT, KEY_MUTE, KEY_RIGHT, KEY_UP, KEY_VOLUMEDOWN, KEY_VOLUMEUP};

use crate::client::Command;

pub fn buttons(sender: Sender<Command>) -> gtk::Box {
    let buttons_box = gtk::BoxBuilder::new()
        .visible(true)
        .orientation(gtk::Orientation::Vertical)
        .build();

    let controls_grid = gtk::GridBuilder::new()
        .visible(true)
        .column_homogeneous(true)
        .build();

    let button_up = button("go-up");
    let s = sender.clone();
    button_up.connect_clicked(move |_| {
        if let Err(e) = s.send(Command::Key(KEY_UP as u16)) {
            println!("{:?}", e);
        }
    });
    let button_down = button("go-down");
    let s = sender.clone();
    button_down.connect_clicked(move |_| {
        if let Err(e) = s.send(Command::Key(KEY_DOWN as u16)) {
            println!("{:?}", e);
        }
    });
    let button_left = button("go-left");
    let s = sender.clone();
    button_left.connect_clicked(move |_| {
        if let Err(e) = s.send(Command::Key(KEY_LEFT as u16)) {
            println!("{:?}", e);
        }
    });
    let button_right = button("go-right");
    let s = sender.clone();
    button_right.connect_clicked(move |_| {
        if let Err(e) = s.send(Command::Key(KEY_RIGHT as u16)) {
            println!("{:?}", e);
        }
    });

    let button_home = button("home");
    let s = sender.clone();
    button_home.connect_clicked(move |_| {
        if let Err(e) = s.send(Command::Key(KEY_HOMEPAGE as u16)) {
            println!("{:?}", e);
        }
    });

    let button_volume_down = button("audio-volume-medium");
    let s = sender.clone();
    button_volume_down.connect_clicked(move |_| {
        if let Err(e) = s.send(Command::Key(KEY_VOLUMEDOWN as u16)) {
            println!("{:?}", e);
        }
    });
    let button_volume_mute = button("audio-volume-muted");
    let s = sender.clone();
    button_volume_mute.connect_clicked(move |_| {
        if let Err(e) = s.send(Command::Key(KEY_MUTE as u16)) {
            println!("{:?}", e);
        }
    });
    let button_volume_up = button("audio-volume-high");
    let s = sender.clone();
    button_volume_up.connect_clicked(move |_| {
        if let Err(e) = s.send(Command::Key(KEY_VOLUMEUP as u16)) {
            println!("{:?}", e);
        }
    });

    controls_grid.attach(&button_up, 1, 0, 1, 1);
    controls_grid.attach(&button_left, 0, 1, 1, 1);
    controls_grid.attach(&button_home, 1, 1, 1, 1);
    controls_grid.attach(&button_right, 2, 1, 1, 1);
    controls_grid.attach(&button_down, 1, 2, 1, 1);

    controls_grid.attach(&button_volume_up, 4, 0, 1, 1);
    controls_grid.attach(&button_volume_mute, 4, 1, 1, 1);
    controls_grid.attach(&button_volume_down, 4, 2, 1, 1);

    buttons_box.append(&controls_grid);

    buttons_box
}

pub fn button(icon: &str) -> gtk::Button {
    let button = gtk::ButtonBuilder::new()
        .icon_name(icon)
        .height_request(50)
        .build();
    button
}
