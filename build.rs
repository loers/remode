extern crate cc;

fn main() {
    cc::Build::new()
        .file("src/server/prepare.c")
        .compile("libprepare.a");
}
